<?php 
include 'koneksi.php';
$page="Edit Barang";
$kode_barang = $_GET['kode_barang'];
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Matrix lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Matrix admin lite design, Matrix admin lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Matrix Admin Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title><?= $page.' - '.$kode_barang ?> | Pengadaan Barang</title>
    <?php include('css.php'); ?>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php include ('header.php'); ?>
        <?php include ('sidebar.php'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title"><?= $page.' | '.$kode_barang; ?></h4>
                        
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-12">
                    <div class="card">
                            <form class="form-horizontal" action="barang_editaksi.php" method="POST">
                            <?php 
                            $data = mysqli_query($koneksi,"SELECT * FROM barang_pengadaan WHERE kode_barang = '$kode_barang'");
                            $result = mysqli_fetch_array($data);
                            ?>    
                            <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-end control-label col-form-label">Nama Barang</label>
                                        <div class="col-sm-9">
                                            <input type="hidden" class="form-control" name="kode_barang" value="<?= $result['kode_barang']; ?>">
                                            <input type="text" class="form-control" name="nama_barang" value="<?= $result['nama_barang']; ?>">
                                        </div>
                                    </div>
                    
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-end control-label col-form-label">Satuan Barang</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="satuan_barang" value="<?= $result['satuan_barang']; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-end control-label col-form-label">Harga Barang</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="harga_barang" value="<?= $result['harga_barang']; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-end control-label col-form-label">Stok Barang</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="stok_barang" value="<?= $result['stok_barang']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <a href="barang_view.php" class="btn btn-primary float-left">Kembali</a>
                                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- column -->

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <?php include ('footer.php'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <?php include ('js.php'); ?>
</body>

</html>